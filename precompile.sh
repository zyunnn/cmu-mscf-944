#! /bin/bash
echo "Precompiled headers disabled, not working as of 2020-01-12"
exit 0

for f in preamble subfile; do
    pdflatex -ini -jobname=$f "&pdflatex $f.tex\\dump"
done
